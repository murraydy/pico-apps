#include "pico/stdlib.h"

/**
 * @brief EXAMPLE - BLINK_C
 *        Simple example to initialise the built-in LED on
 *        the Raspberry Pi Pico and then flash it forever. 
 * 
 * @return int  Application return code (zero for success).
 * 
 */

int blink(int ledPin, int ledDelay);

int main() {
    
    blink(25, 500);

    // Should never get here due to infinite while-loop.
    return 0;
}

/**
 * @brief blink will take two paramters and switch between
 *        on and off based upon these.
 * 
 * @param ledPin the pin that the led is connected to. 
 * @param ledDelay the delay between on a off
 * @return int subroutine return code (zero for success)
 */
int blink(int ledPin, int ledDelay) {
    int isOn = 0;
    gpio_init(ledPin);
    gpio_set_dir(ledPin, GPIO_OUT);

    while (true) {
        isOn = 1 - isOn; // 1 - 1 = 0, 1 - 0 = 1
        // Toggle the LED on and then sleep for delay period
        gpio_put(ledPin, isOn);
        sleep_ms(ledDelay);
    }
    return 0;
};

