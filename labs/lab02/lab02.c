//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/float.h"

float findPi(int iterations);
double findPiDouble(int iterations);

const float PI = 3.14159265359;

/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */
int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif
    float piFloat = findPi(100000);
    //The accuracy for the float calculation of pi is calculated as double for more precision.
    double piFloatAccuracy = ((piFloat*100000000000)/(PI*100000000000))*100;

    double piDouble = findPiDouble(100000);
    double piDoubleAccuracy = ((piDouble)/PI)*100;
    // Print the calculated value of pi in both float and double formats.
    printf("Float: %14.12f - Accuracy: %14.11f\nDouble: %14.12f - Accuracy: %14.11f\n", piFloat, piFloatAccuracy, piDouble, piDoubleAccuracy);
    // Returning zero indicates everything went okay.
    return 0;
}

/**
 * @brief findPi
 *       Calculate the value of Pi using the Walls formula.
 * 
 * @param iterations    Number of iterations to perform.
 * 
 * @return float        Value of Pi.
*/
float findPi(int iterations) {
    float pi = 1;
    for(int i = 1; i < iterations; i++) {
        pi *= (2.0 * i)/((2.0 * i) - 1) * (2.0 * i)/((2.0 * i) + 1);
        //printf("%d -- %f\n", i, pi);
        if(i % 10000 == 0) {
          printf("Float calculation: %d%%\n", i/1000);
        }
    }
    printf("Float calculation: COMPLETE\n");
    return 2*pi;
}

/**
 * @brief findPiDouble
 *       Calculate the value of Pi using the Walls formula.
 * 
 * @param iterations    Number of iterations to perform.
 * 
 * @return double       Value of Pi.
*/
double findPiDouble(int iterations) {
    double pi = 1;
    for(int i = 1; i < iterations; i++) {
        pi *= (2.0 * i)/((2.0 * i) - 1) * (2.0 * i)/((2.0 * i) + 1);
        //printf("%d -- %f\n", i, pi);
        if(i % 10000 == 0) {
          printf("Double calculation: %d%%\n", i/1000);
        }
    }
    printf("Double calculation: COMPLETE\n");
    return 2*pi;
}

