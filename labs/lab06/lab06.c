#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h"     // Required for using single-precision variables.
#include "pico/double.h"    // Required for using double-precision variables.
#include "pico/multicore.h"


float findPi(int iterations);
double findPiDouble(int iterations);

const float PI = 3.14159265359;

/**
 * @brief This function acts as the main entry-point for core #1.
 *        A function pointer is passed in via the FIFO with one
 *        incoming int32_t used as a parameter. The function will
 *        provide an int32_t return value by pushing it back on 
 *        the FIFO, which also indicates that the result is ready.
 */

void core1_entry() {
    while (1) {
        // 
        int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking();
        int32_t p = multicore_fifo_pop_blocking();
        int32_t result = (*func)(p);
        multicore_fifo_push_blocking(result);
    }
}


int main() {

    stdio_init_all();

    int sequentialPiFloat = time_us_32(), sequential = sequentialPiFloat; // Start the timer.
    float piFloat = findPi(100000);
    int  sequentialPiFloatEnd = time_us_32(); // Stop the timer.
    //The accuracy for the float calculation of pi is calculated as double for more precision.
    double piFloatAccuracy = ((piFloat*100000000000)/(PI*100000000000))*100;

    int sequentialPiDouble = time_us_32(); // Start the timer.
    double piDouble = findPiDouble(100000);
    int sequentialEnd = time_us_32(); // Stop the timer.
    double piDoubleAccuracy = ((piDouble)/PI)*100;

    int sequentialTime = sequentialEnd - sequential;
    int sequentialPiFloatTime = sequentialPiFloatEnd - sequentialPiFloat;
    int sequentialPiDoubleTime = sequentialEnd - sequentialPiDouble;

    // Print the calculated value of pi in both float and double formats.
    printf("Sequential Float: %14.12f - Accuracy: %14.11f\nSequential Double: %14.12f - Accuracy: %14.11f\n", piFloat, piFloatAccuracy, piDouble, piDoubleAccuracy);
    printf("Sequential Float Time: %d, Sequential Double Time: %d, Total Time: %d", sequentialPiFloatTime, sequentialPiDoubleTime, sequentialTime);

    multicore_launch_core1(core1_entry);
    
    int multicorePiFloat = time_us_32(), multicore = multicorePiFloat;
    multicore_fifo_push_blocking((uintptr_t) &findPi);
    multicore_fifo_push_blocking(100000);
    float multicorePiFloatResult = multicore_fifo_pop_blocking();
    int multicorePiFloatEnd = time_us_32(); // Stop the timer.
    double multicorePiFloatAccuracy = ((multicorePiFloatResult)/PI)*100;

    int multicorePiDouble = time_us_32(); // Start the timer.
    multicore_fifo_push_blocking((uintptr_t) &findPiDouble);
    multicore_fifo_push_blocking(100000);
    double multicorePiDoubleResult = multicore_fifo_pop_blocking();
    int multicorePiDoubleEnd = time_us_32(); // Stop the timer.
    double multicorePiDoubleAccuracy = ((multicorePiDoubleResult)/PI)*100;

    int multicoreTime = multicorePiDoubleEnd - multicore;
    int multicorePiFloatTime = multicorePiFloatEnd - multicorePiFloat;
    int multicorePiDoubleTime = multicorePiDoubleEnd - multicorePiDouble;


    printf("Multicore Float Time: %d, Multicore Double Time: %d, Total Time: %d", multicorePiFloatTime, multicorePiDoubleTime, multicoreTime);


    // Returning zero indicates everything went okay.
    return 0;
}

/**
 * @brief findPi
 *       Calculate the value of Pi using the Walls formula.
 * 
 * @param iterations    Number of iterations to perform.
 * 
 * @return float        Value of Pi.
*/
float findPi(int iterations) {
    float pi = 1;
    for(int i = 1; i < iterations; i++) {
        pi *= (2.0 * i)/((2.0 * i) - 1) * (2.0 * i)/((2.0 * i) + 1);
        //printf("%d -- %f\n", i, pi);
        if(i % 10000 == 0) {
          printf("Float calculation: %d%%\n", i/1000);
        }
    }
    printf("Float calculation: COMPLETE\n");
    return 2*pi;
}

/**
 * @brief findPiDouble
 *       Calculate the value of Pi using the Walls formula.
 * 
 * @param iterations    Number of iterations to perform.
 * 
 * @return double       Value of Pi.
*/
double findPiDouble(int iterations) {
    double pi = 1;
    for(int i = 1; i < iterations; i++) {
        pi *= (2.0 * i)/((2.0 * i) - 1) * (2.0 * i)/((2.0 * i) + 1);
        //printf("%d -- %f\n", i, pi);
        if(i % 10000 == 0) {
          printf("Double calculation: %d%%\n", i/1000);
        }
    }
    printf("Double calculation: COMPLETE\n");
    return 2*pi;
}

